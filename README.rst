Medaka GeneFlow App
===================

Version: 0.11.5-01

This GeneFlow app wraps the medaka tool for polishing nanopore assemblies.  

Inputs
------
1. input: Sequence FASTQ file.  
2. assembly: Assembled contigs (or unitigs).  

Parameters
----------
1. model: Sequence model to use for correction.
2. output: Output directory.
